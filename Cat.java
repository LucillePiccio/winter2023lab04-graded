public class Cat{
	private String color;
	private int age;
	private String personality;
	private String food;
	
	//Lab04 Adding constructor methods
	public Cat(String color, int age, String personality, String food){
		this.color = "no color entered";
		this.age = 0;
		this.personality = "no personality entered";
		this.food = "no food entered";
	}
	//method will take in the cat's age and will print its current action
	public void action(){
		if(this.age < 18){
			System.out.println("The cat is playing with its toys");
		}
		else if(this.age == 18){
			System.out.println("The cat is sitting on the couch and watching our tv show");
		}
		//when age > 18
		else{
			System.out.println("The cat is sleeping");
		}
	}
	//method takes in the cats personality and will print what sound it made
	public void sound(){
		if(this.personality.equals("lazy")){
			System.out.print("The cat went 'hiss'");
		}
		else if(this.personality.equals("affectionate")){
			System.out.print("The cat went 'meow'");
		}
		//when personality is antisocial
		else{
			System.out.println("The cat went 'purr'");
		}
	}
	
	//Lab4 starts here
	public void updateFood(String input){
		if(isFoodDry(input)){
			System.out.println(this.food = "The cat's food is dry");
		}
		else{
			System.out.println(this.food = "The cat's food is wet");
		}
	}
	//helper method for updateFood method. Checks if the food is dry or wet and returns boolean
	private boolean isFoodDry(String input){
		boolean isDry = input.equals("dry");
	if(input.equals("dry")){
		isDry = true;
		}
		return isDry;
	}
	
	//adding setter methods here
	public void setColor(String newColor){
		this.color = newColor;
	}
	public void setAge(int newAge){
		this.age = newAge;
	}
	public void setPersonality(String newPersonality){
		this.personality = newPersonality;
	}
	public void setFood(String newFood){
		this.food = newFood;
	}
	
	//adding getter methods here
	public String getColor(){
		return color;
	}
	public int getAge(){
		return age;
	}
	public String getPersonality(){
		return personality;
	}
	public String getFood(){
		return food;
	}
}