import java.util.Scanner;
public class NationalPark{
	public static void main(String[]args){
		
		Cat[] clowderOfCats = new Cat[4];
		//filling in the array using a for loop
		for(int i = 0; i < clowderOfCats.length; i++){
			Scanner reader = new Scanner(System.in);
			Cat cat = new Cat();
			clowderOfCats[i] = cat; //cat1 added to clowerOfCats[i]
			
			//updated with new setter and getter methods for Lab04
			System.out.println("Tell me about your cats color");
			String color = reader.nextLine();
			clowderOfCats[i].setColor(color);
			
			System.out.println("How old is the cat?");
			int age = Integer.parseInt(reader.nextLine());
			clowderOfCats[i].setAge(age);
			
			System.out.println("How about its personality? Is it affectionate, lazy or antisocial?");
			String personality = reader.nextLine();
			clowderOfCats[i].setPersonality(personality);
			
			//add new food field from lab4
			System.out.println("Is your cat's food wet or dry?");
			String food = reader.nextLine();
			clowderOfCats[i].setFood(food);
		}
		Scanner reader = new Scanner(System.in);
		
		//printing 3 fields of the last cat
		//changed methods to getter methods
		System.out.println("fields before the setter methods");
		System.out.println(clowderOfCats[3].getColor());
		System.out.println(clowderOfCats[3].getAge());
		System.out.println(clowderOfCats[3].getPersonality());
		System.out.println(clowderOfCats[3].getFood());
		
		System.out.println("Here are the informations about the last cat");
		System.out.println("Tell me the color of the cat");
		String newColor = reader.nextLine();
		clowderOfCats[3].setColor(newColor);
		
		System.out.println("Tell me about the cat's age");
		int newAge = Integer.parseInt(reader.nextLine());
		clowderOfCats[3].setAge(newAge);
		
		System.out.println("Tell me about the cat's personality");
		String newPersonality = reader.nextLine();
		clowderOfCats[3].setPersonality(newPersonality);
		
		System.out.println("Tell me about your cat's food");
		String newFood = reader.nextLine();
		clowderOfCats[3].setFood(newFood);
		
		System.out.println("fields after the setter methods");
		System.out.println(clowderOfCats[3].getColor());
		System.out.println(clowderOfCats[3].getAge());
		System.out.println(clowderOfCats[3].getPersonality());
		System.out.println(clowderOfCats[3].getFood());
		
		
		//calling 2 instance methods from cat.java of the first cat
		System.out.println("About the first cat:");
		clowderOfCats[0].action();
		clowderOfCats[0].sound();
		
		//lab4, calling updateFood method on second cat
		System.out.println("About the second cat:");
		System.out.println("Is your second cat's food wet or dry?");
		String food = reader.nextLine();
		clowderOfCats[1].updateFood(food);
	}
	
}